package com.teamplus.demo.data.models

data class Apps (

	val name : String,
	val currency : String,
	val moneyFormat : String,
	val data : Data
)