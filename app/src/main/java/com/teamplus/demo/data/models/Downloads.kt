package com.teamplus.demo.data.models

data class Downloads (

	val total : Long,
	val month_wise : Month_wise
)