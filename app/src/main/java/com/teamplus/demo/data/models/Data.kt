package com.teamplus.demo.data.models

data class Data (

	val downloads : Downloads,
	val sessions : Sessions,
	val total_sale : Total_sale,
	val add_to_cart : Add_to_cart
)