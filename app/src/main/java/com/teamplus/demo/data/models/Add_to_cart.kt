package com.teamplus.demo.data.models

data class Add_to_cart (

	val total : Long,
	val month_wise : Month_wise
)