package com.teamplus.demo.data.repositories

import com.teamplus.demo.data.network.Api

class CompaniesRepository(
    private val api: Api
) : SafeApiRequest() {
    
    suspend fun getCompanies() = apiRequest { api.getCompanies() }

}