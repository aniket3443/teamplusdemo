package com.teamplus.demo.data.models

data class Month_wise (

	val jan : Int,
	val feb : Int,
	val mar : Int,
	val apr : Int,
	val may : Int,
	val jun : Int
)