package com.teamplus.demo.data.network

import com.teamplus.demo.data.models.Company
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit


interface Api {

    @GET("dummy-app-data.json")
    suspend fun getCompanies() : Response<Company>

    companion object{
        operator fun invoke() : Api {
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://plobalapps.s3-ap-southeast-1.amazonaws.com/")
                .callFactory(okHttpClient)
                .build()
                .create(Api::class.java)
        }
    }
}
