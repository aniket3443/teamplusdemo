package com.teamplus.demo.data.models

data class Total_sale (

	val total : Long,
	val month_wise : Month_wise
)