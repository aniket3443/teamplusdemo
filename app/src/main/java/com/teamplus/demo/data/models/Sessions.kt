package com.teamplus.demo.data.models

data class Sessions (

	val total : Long,
	val month_wise : Month_wise
)