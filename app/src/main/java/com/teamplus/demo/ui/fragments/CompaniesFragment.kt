package com.teamplus.demo.ui.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.teamplus.demo.R
import com.teamplus.demo.data.models.Apps
import com.teamplus.demo.data.network.Api
import com.teamplus.demo.data.repositories.CompaniesRepository
import com.teamplus.demo.databinding.CompaniesFragmentBinding
import com.teamplus.demo.databinding.GraphFragmentBinding
import com.teamplus.demo.databinding.SortFragmentBinding
import com.teamplus.demo.ui.adapter.CompaniesAdapter
import com.teamplus.demo.ui.listener.RecyclerViewClickListener
import com.teamplus.demo.util.SharedPreferenceManager
import kotlinx.android.synthetic.main.companies_fragment.*
import java.util.*

class CompaniesFragment : Fragment(), RecyclerViewClickListener {

    private lateinit var companiesViewModelFactory: CompaniesViewModelFactory
    private lateinit var companiesViewModel: CompaniesViewModel
    private lateinit var companiesFragmentBinding: CompaniesFragmentBinding
    private var dialog: BottomSheetDialog? = null
    private val nullParent: ViewGroup? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        companiesFragmentBinding = CompaniesFragmentBinding.inflate(inflater, container, false)
        return companiesFragmentBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("test","in onActivityCreated")
        companiesFragmentBinding.progressBar.visibility = View.VISIBLE

        val api = Api()
        val repository = CompaniesRepository(api)

        companiesViewModelFactory = CompaniesViewModelFactory(repository)
        companiesViewModel =
            ViewModelProvider(this, companiesViewModelFactory).get(CompaniesViewModel::class.java)

        if (isNetworkAvailable(requireContext())) {
            companiesViewModel.getCompanies()
        } else {
            //TODO: pull down to refresh list
            companiesFragmentBinding.errorTextView.visibility = View.VISIBLE
            companiesFragmentBinding.progressBar.visibility = View.GONE
        }

        companiesViewModel.companiesLiveData.observe(viewLifecycleOwner, Observer { companies ->
            recycler_view_companies.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter = CompaniesAdapter(companies, this)
                companiesFragmentBinding.progressBar.visibility = View.GONE
                companiesFragmentBinding.sortTextView.visibility = View.VISIBLE
            }
        })

        companiesFragmentBinding.sortTextView.setOnClickListener {
            showSortingBottomSheetDialog()
        }
    }

    override fun onRecyclerViewItemClick(view: View, company: Apps) {
        when (view.id) {
            R.id.viewDetailsButton -> {
                showGraphBottomSheetDialog(company)
            }
        }
    }

    private fun showSortingBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.sort_fragment, nullParent)

        val sortFragmentBinding = SortFragmentBinding.bind(view)
        dialog = BottomSheetDialog(requireContext())
        dialog?.setContentView(view)
        //check radio button
        setCheckRadio(sortFragmentBinding)

        sortFragmentBinding.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            val radio: RadioButton = sortFragmentBinding.root.findViewById(checkedId)
            SharedPreferenceManager.saveStringPreference(
                requireContext(),
                R.string.sort_by,
                radio.text.toString()
            )
            sortAccordingToRadioButton(radio.text.toString())
        }

        dialog?.show()
    }

    private fun showGraphBottomSheetDialog(company: Apps) {
        val view = layoutInflater.inflate(R.layout.graph_fragment, nullParent)
        val graphFragmentBinding = GraphFragmentBinding.bind(view)
        graphFragmentBinding.company = company
        dialog = BottomSheetDialog(requireContext())
        dialog?.setContentView(view)

        val totalSalesList = companiesViewModel.getTotalSalesList(company)

        val lineData = LineData(getLineData(totalSalesList))
        graphFragmentBinding.lineChart.data = lineData
        graphFragmentBinding.lineChart.axisRight.isEnabled = false
        graphFragmentBinding.lineChart.setTouchEnabled(true)
        graphFragmentBinding.lineChart.setPinchZoom(true)
        //hide legend
        graphFragmentBinding.lineChart.legend.isEnabled = false
        //hide description
        graphFragmentBinding.lineChart.description = null

        setupXAxis(graphFragmentBinding)
        setupYAxis(graphFragmentBinding)

        dialog?.show()
    }

    private fun setCheckRadio(sortFragmentBinding: SortFragmentBinding) {
        when (SharedPreferenceManager.getStringPreference(requireContext(), R.string.sort_by)) {
            getString(R.string.total_sales) -> {
                sortFragmentBinding.radioGroup.check(R.id.totalSalesRadioButton)
            }
            getString(R.string.add_to_cart) -> {
                sortFragmentBinding.radioGroup.check(R.id.addToCartRadioButton)
            }
            getString(R.string.downloads) -> {
                sortFragmentBinding.radioGroup.check(R.id.downloadsRadioButton)
            }
            getString(R.string.user_sessions) -> {
                sortFragmentBinding.radioGroup.check(R.id.userSessionsRadioButton)
            }
        }
    }

    private fun sortAccordingToRadioButton(radioButtonClickedText: String) {
        when (radioButtonClickedText) {
            getString(R.string.total_sales) -> {
                companiesViewModel.sortByTotalSales()
                dialog?.dismiss()
            }
            getString(R.string.add_to_cart) -> {
                companiesViewModel.sortByAddToCart()
                dialog?.dismiss()
            }
            getString(R.string.downloads) -> {
                companiesViewModel.sortByDownloads()
                dialog?.dismiss()
            }
            getString(R.string.user_sessions) -> {
                companiesViewModel.sortByUserSessions()
                dialog?.dismiss()
            }
        }
    }

    private fun getLineData(totalSalesList: ArrayList<Entry>): ArrayList<ILineDataSet> {
        val lineDataSet = LineDataSet(totalSalesList, null)
        lineDataSet.values = totalSalesList
        lineDataSet.enableDashedLine(10f, 5f, 0f)
        lineDataSet.enableDashedHighlightLine(10f, 5f, 0f)
        lineDataSet.color = Color.DKGRAY
        lineDataSet.setCircleColor(Color.DKGRAY)
        lineDataSet.setDrawIcons(false)
        lineDataSet.setDrawCircleHole(false)
        lineDataSet.setDrawFilled(true)
        lineDataSet.lineWidth = 1f
        lineDataSet.circleRadius = 3f
        lineDataSet.valueTextSize = 9f
        lineDataSet.formLineWidth = 1f
        lineDataSet.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        lineDataSet.formSize = 15f
        lineDataSet.fillDrawable =
            ContextCompat.getDrawable(requireContext(), R.drawable.gradient_blue_background)

        val lineDataSetList = ArrayList<ILineDataSet>()
        lineDataSetList.add(lineDataSet)

        return lineDataSetList
    }

    private fun setupXAxis(graphFragmentBinding: GraphFragmentBinding) {
        val xAxis: XAxis = graphFragmentBinding.lineChart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawLimitLinesBehindData(true)
        xAxis.labelCount = 6
        xAxis.granularity = 1f
        val xAxisLabel = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun")
        xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabel)
    }

    private fun setupYAxis(graphFragmentBinding: GraphFragmentBinding) {
        val leftAxis: YAxis = graphFragmentBinding.lineChart.axisLeft
        leftAxis.isEnabled = false
        leftAxis.setDrawLimitLinesBehindData(false)
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork ?: return false
        val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
        return when {
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }

}