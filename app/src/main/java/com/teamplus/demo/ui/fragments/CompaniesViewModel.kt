package com.teamplus.demo.ui.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.mikephil.charting.data.Entry
import com.teamplus.demo.data.models.Apps
import com.teamplus.demo.data.repositories.CompaniesRepository
import com.teamplus.demo.util.Coroutines
import kotlinx.coroutines.Job

class CompaniesViewModel(
    private val repository: CompaniesRepository
) : ViewModel() {
    private lateinit var job: Job
    private val companiesMutableLiveData = MutableLiveData<List<Apps>>()
    private val appsListMutableLiveData = mutableListOf<Apps>()

    val companiesLiveData: LiveData<List<Apps>>
        get() = companiesMutableLiveData

    fun getCompanies() {
        job = Coroutines.ioThenMain(
            { repository.getCompanies() },
            {
                companiesMutableLiveData.value = it?.apps
                appsListMutableLiveData.clear()
                it?.apps?.forEach { apps ->
                    appsListMutableLiveData.add(apps)
                }
            }
        )
    }

    fun sortByAddToCart() {
        appsListMutableLiveData.sortBy { it.data.add_to_cart.total }
        companiesMutableLiveData.value = appsListMutableLiveData
    }

    fun sortByTotalSales() {
        appsListMutableLiveData.sortBy { it.data.total_sale.total }
        companiesMutableLiveData.value = appsListMutableLiveData
    }

    fun sortByDownloads() {
        appsListMutableLiveData.sortBy { it.data.downloads.total }
        companiesMutableLiveData.value = appsListMutableLiveData
    }

    fun sortByUserSessions() {
        appsListMutableLiveData.sortBy { it.data.sessions.total }
        companiesMutableLiveData.value = appsListMutableLiveData
    }

    fun getTotalSalesList(company: Apps): ArrayList<Entry> {
        val totalSalesArrayList = ArrayList<Entry>()
        totalSalesArrayList.add(Entry(0f, company.data.total_sale.month_wise.jan.toFloat()))
        totalSalesArrayList.add(Entry(1f, company.data.total_sale.month_wise.feb.toFloat()))
        totalSalesArrayList.add(Entry(2f, company.data.total_sale.month_wise.mar.toFloat()))
        totalSalesArrayList.add(Entry(3f, company.data.total_sale.month_wise.apr.toFloat()))
        totalSalesArrayList.add(Entry(4f, company.data.total_sale.month_wise.may.toFloat()))
        totalSalesArrayList.add(Entry(5f, company.data.total_sale.month_wise.jun.toFloat()))
        return totalSalesArrayList
    }
    //TODO: add function to get list(addToCart, Download, userSession) for graph

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}
