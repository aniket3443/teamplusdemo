package com.teamplus.demo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.teamplus.demo.R
import com.teamplus.demo.data.models.Apps
import com.teamplus.demo.databinding.ItemRowCompanyBinding
import com.teamplus.demo.ui.listener.RecyclerViewClickListener

class CompaniesAdapter (
    private val companies: List<Apps>,
    private val listener: RecyclerViewClickListener
) : RecyclerView.Adapter<CompaniesAdapter.CompaniesViewHolder>(){

    override fun getItemCount() = companies.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CompaniesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_row_company,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CompaniesViewHolder, position: Int) {
        holder.itemRowCompanyBinding.company = companies[position]
        holder.itemRowCompanyBinding.viewDetailsButton.setOnClickListener {
            listener.onRecyclerViewItemClick(holder.itemRowCompanyBinding.viewDetailsButton, companies[position])
        }
    }

    inner class CompaniesViewHolder(
        val itemRowCompanyBinding : ItemRowCompanyBinding
    ) : RecyclerView.ViewHolder(itemRowCompanyBinding.root)

}