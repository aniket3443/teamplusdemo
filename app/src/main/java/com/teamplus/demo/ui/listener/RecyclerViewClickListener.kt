package com.teamplus.demo.ui.listener

import android.view.View
import com.teamplus.demo.data.models.Apps

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View, company: Apps)
}