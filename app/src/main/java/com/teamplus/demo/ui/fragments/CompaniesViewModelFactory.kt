package com.teamplus.demo.ui.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.teamplus.demo.data.repositories.CompaniesRepository

@Suppress("UNCHECKED_CAST")
class CompaniesViewModelFactory(
    private val repository: CompaniesRepository
) : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CompaniesViewModel(repository) as T
    }

}